<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Formulir Hasil Tes Kesehatan - branch develop</title>


        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="{{ asset('bootstrap_3.3.7/css/bootstrap.min.css')}}"  rel="stylesheet">

        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Open Sans';
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
      <!--
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    <marquee>SISTEM-VERIFIKASI-BANK-BTN</marquee>
                </div>
            </div>
        </div>
      -->
        <div class="container">

          <form class="form-horizontal" action="" method="post">
            {{ csrf_field() }}
            <hr>
            <center><h3>FORM KESEHATAN</h3></center>
            <hr>
            <div class="form-group">
              <label for="" class="col-md-offset-1 col-sm-2 control-label">Nama</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="nama" value="" placeholder="Nama Peserta">
              </div>
            </div>

            <div class="form-group">
<<<<<<< HEAD
              <label for="" class="col-md-offset-1 col-sm-2 control-label">Alamat</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="nama" value="" placeholder="Nama Peserta">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Jenis Kelamin</label>
              <div class="col-sm-2">
                <input type='radio'  name='jenis_kelamin' value='laki-laki' />Laki-Laki
              </div>
              <div class="col-sm-2">
                <input type='radio'  name='jenis_kelamin' value='perempuan' />Perempuan
              </div>
            </div>

            <div class="form-group">
=======
>>>>>>> develop
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Status</label>
              <div class="col-sm-2">
                <input type='radio'  name='status' value='menikah' />Menikah
              </div>
              <div class="col-sm-2">
                <input type='radio'  name='status' value='belummenikah' />Belum Menikah
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Umur(Tahun)</label>
              <div class="col-sm-1">
                <input type="number" min="15" max="21" name="umur" class="form-control" value="">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Tinggi Badan(cm)</label>
              <div class="col-sm-1">
                <input type="number" min="130" max="220" class="form-control" name="tinggi" value="">
              </div> 
            </div>
            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Berat Badan(kg)</label>
              <div class="col-sm-1">
                <input type="number" min="40" max="100" class="form-control" name="berat_badan" value="">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Tekanan Darah(mmHG)</label>
              <div class="col-xs-1">
                <input type="number" class="form-control" name="tekanan_darah_1" value="">
              </div> 
              <div class="col-xs-1" width="10px">
               <p class="form-control-static text-center">/</p>
              </div> 
              <div class="col-xs-1">
                <input type="number" class="form-control" name="tekanan_darah_2" value="">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Nadi(x/menit)</label>
              <div class="col-sm-1">
                <input type="number" class="form-control" name="nadi" value="">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">P.Fisik</label>
              <div class="col-sm-2">
                <p><input type='radio' name='pfisik' value='' />Baik</p>
              </div>
              <div class="col-sm-2">
                <p><input type='radio' name='pfisik' value='' />Tidak Baik</p>
              </div> 
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Audio Metri</label>
              <div class="col-sm-2">
                <p><input type='radio' name='audiometri' value='' />Baik</p>
              </div>
              <div class="col-sm-2">
                <p><input type='radio' name='audiometri' value='' />Tidak Baik</p>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Hasil Lab-urin</label>
              <div class="col-sm-2">
                <p><input type='radio' name='urin' value='' />Baik</p>
              </div>
              <div class="col-sm-2">
                <p><input type='radio' name='urin' value='' />Tidak Baik</p>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Hasil Lab-Darah</label>
              <div class="col-sm-2">
                <p><input type='radio' name='darah' value='' />Baik</p>
              </div>
              <div class="col-sm-2">
                <p><input type='radio' name='darah' value='' />Tidak Baik</p>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Hasil Lab-HbsAg</label>
              <div class="col-sm-2">
                <p><input type='radio' name='hbsag' value='' />Baik</p>
              </div>
              <div class="col-sm-2">
                <p><input type='radio' name='hbsag' value='' />Tidak Baik</p>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Narkoba</label>
              <div class="col-sm-2">
                <p><input type='radio' name='narkoba' value='' />Baik</p>
              </div>
              <div class="col-sm-2">
                <p><input type='radio' name='narkoba' value='' />Tidak Baik</p>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Tes Hamil</label>
              <div class="col-sm-2">
                <p><input type='radio' name='hamil' value='' />Positif</p>
              </div>
              <div class="col-sm-2">
                <p><input type='radio' name='hamil' value='' />Negatif</p>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">EKG</label>
              <div class="col-sm-2">
                <p><input type='radio' name='ekg' value='' />Baik</p>
              </div>
              <div class="col-sm-2">
                <p><input type='radio' name='ekg' value='' />Tidak Baik</p>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Rontgen</label>
              <div class="col-sm-2">
                <p><input type='radio' name='rontgen' value='' />Baik</p>
              </div>
              <div class="col-sm-2">
                <p><input type='radio' name='rontgen' value='' />Tidak Baik</p>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">THT</label>
              <div class="col-sm-2">
                <p><input type='radio' name='tht' value='' />Baik</p>
              </div>
              <div class="col-sm-2">
                <p><input type='radio' name='tht' value='' />Tidak Baik</p>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Gigi</label>
              <div class="col-sm-2">
                <p><input type='radio' name='gigi' value='' />Baik</p>
              </div>
              <div class="col-sm-2">
                <p><input type='radio' name='gigi' value='' />Tidak Baik</p>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Visus Mata</label>
              <div class="col-xs-1">
                <input type="text" class="form-control" name="mata_kanan_1" value="">
              </div> 
              <div class="col-xs-1" width="10px">
               <p class="form-control-static text-center">/</p>
              </div> 
              <div class="col-xs-1">
                <input type="text" class="form-control" name="mata_kanan_2" value="">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Visus Mata Kiri</label>
              <div class="col-xs-1">
                <input type="text" class="form-control" name="mata_kiri_1" value="">
              </div> 
              <div class="col-xs-1" width="10px">
               <p class="form-control-static text-center">/</p>
              </div> 
              <div class="col-xs-1">
                <input type="text" class="form-control" name="mata_kiri_2" value="">
              </div>
            </div>
            <hr>
            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Ishihara Test</label>
              <div class="col-sm-2">
                <p><input type='radio' name='hbsag' value='' />Baik</p>
              </div>
              <div class="col-sm-2">
                <p><input type='radio' name='hbsag' value='' />Tidak Baik</p>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Hasil Test</label>
              <div class="col-sm-2">
                <p><input type='radio' name='hbsag' value='' />Baik</p>
              </div>
              <div class="col-sm-2">
                <p><input type='radio' name='hbsag' value='' />Tidak Baik</p>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-offset-1 col-sm-2 control-label" for="">Keterangan</label>
              <div class="col-sm-6">
                <textarea class="form-control" name="keterangan" rows="3"></textarea>
              </div>
            </div>
            <div class="form-group ">
              <div class="col-md-offset-4 col-sm-4">
                <input type="submit" class="col-md-offset-1 btn btn-primary"  value="SIMPAN DATA">
              </div>
            </div>
          </form>
        </div>

    </body>
</html>
